var dinos = [];
var N = 10;
cont = 0;
var cont_frames = 0;
var delay = 10;
function setup() {
    createCanvas(1000, 1000);
    for (let i = 1; 1 <= N; i++){
        dinos.push(loadImage("imgs/Walk ("+i+").png"))
    }

  }
  
  function draw() {
      cont_frames++;
      if (!(cont_frames % delay)){
        background(000);

        let img = dinos[cont];
        image(img,15+cont_frames,10,img.width/50,img.height/50);
    
        cont++;
        if(cont >= N){
            cont = 0;
        }
        if(cont_frames >= 1000){
            cont_frames = 0;
        }
      }
  }
  